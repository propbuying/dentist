@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            @if(session('msg'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('msg')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
            <div class="k-portlet k-portlet--mobile">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title">
                            List Of Modules
                        </h3>
                    </div>
                </div>
                <div class="k-portlet__body">
                <div class="col-md-4">
                    <a href="/modules/create" class="btn btn-info">Add New</a>
                </div>
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="k_table_1">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                                @if(count($modules)>0)
                                @foreach($modules as $module)
                                <tr>
                                        <td>{{$module->id}}</td>
                                        <td>{{$module->name}}</td>
                                        <td >
                                            <a href="/permissions/{{$module->id}}/edit"><i class="flaticon2-edit-interface-symbol-of-pencil-tool"></i></a> | <a href="javascript:void(0)" data-id="{{$module->id}}" class="k_sweetalert_demo_8"><i class="flaticon-delete"></i></a>
                                            <form method="post" action="/destroypermission/{{$module->id}}" class="permission_{{$module->id}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif        
                        </tbody>
                    </table>

                    <!--end: Datatable -->
                </div>
            </div>
            
        </div>

        <!-- end:: Content Body -->
@endsection