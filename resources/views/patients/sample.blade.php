@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            @if(session('msg'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('msg')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
            <div class="k-portlet k-portlet--mobile">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title">
                            List Of Patients
                        </h3>
                    </div>
                </div>
                <div class="k-portlet__body">
                        <div class='alert alert-success fade show' role='alert' style="display:none;">
                                <div class='alert-icon'><i class='fa fa-check'></i></div>
                                <div class='alert-text' id="patientmsg"></div>
                                <div class='alert-close'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                        <span aria-hidden='true'><i class='la la-close'></i></span>
                                    </button>
                                </div>
                            </div>
              
               <div class="col-md-4">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" class="btn btn-info">Add New</a>
                </div>
                <!-- Modal -->
               <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Patient</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="addPatient" class="k-form" >
                                    @csrf
                                    
                                    <div class="k-portlet__body">
                                        
                                        <div class="form-group">
                                            <label>Name</label>
                                        <input type="text" class="form-control" name="name" {{ $errors->has('name') ? 'has-error' : '' }}  value="{{old('name')}}" placeholder="Enter role">
                                        <span class="help-block"><strong></strong></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile Number </label>
                                        <input type="phone" class="form-control" name="mobile" {{ $errors->has('mobile') ? 'has-error' : '' }}  value="{{old('mobile')}}" placeholder="Enter Patient's Mobile">
                                        <span class="help-block"><strong></strong></span> 
                                        </div>
                                        <div class="form-group">
                                            <label>Age </label>
                                        <input type="text" class="form-control" name="age" {{ $errors->has('age') ? 'has-error' : '' }}  value="{{old('age')}}" placeholder="Enter Patient's age">
                                        <span class="help-block"><strong></strong></span>
                                        </div>
                                    </div>
                                    
                                
        
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" id="closepatient" data-dismiss="modal">Close</button>
                                <button type="button" id="patientsubmit" class="btn btn-brand ">Save changes</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                    <!--begin: Datatable --> @if(count($patients)>0)
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="k_table_1">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Age</th>
                                <th>Treatment</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                               
                                @foreach($patients as $patient)
                                <tr>
                                        <td>{{$patient->id}}</td>
                                        <td>{{$patient->name}}</td>
                                        <td>{{$patient->mobile}}</td>
                                        <td>{{$patient->age}}</td>
                                        <td>{{$patient->treatment}}</td>
                                        <td >
                                              <a href="/patients/{{$patient->id}}/edit"><i class="flaticon2-edit-interface-symbol-of-pencil-tool"></i></a> | <a href="javascript:void(0)" data-id="{{$patient->id}}" class="k_sweetalert_demo_8"><i class="flaticon-delete"></i></a>
                                            <form method="post" action="/patients/{{$patient->id}}" class="patient_{{$patient->id}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                   
                        </tbody>
                    </table>
                    @else
                        <div class="col-md-12 text-center">
                            <span class="nodata">No Data found</span>
                        </div>
                    
                    @endif 
                    <!--end: Datatable -->
                </div>
            </div>
            
        </div>

        <!-- end:: Content Body -->
@endsection