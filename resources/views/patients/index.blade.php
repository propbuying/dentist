@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
           
            <div class="k-portlet k-portlet--mobile">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title">
                            List Of Patients
                        </h3>
                    </div>
                </div>
                <div class="k-portlet__body">
                        <div class='alert alert-success fade show' role='alert' style="display:none;">
                                <div class='alert-icon'><i class='fa fa-check'></i></div>
                                <div class='alert-text' id="patientmsg"></div>
                                <div class='alert-close'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                        <span aria-hidden='true'><i class='la la-close'></i></span>
                                    </button>
                                </div>
                            </div>
              
               <div class="col-md-4">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" class="btn btn-info">Add New</a>
                </div>
                <!-- Modal -->
               <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Patient</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="addPatient" class="k-form" >
                                    @csrf
                                    
                                    <div class="k-portlet__body">
                                        
                                        <div class="form-group">
                                            <label>Name</label>
                                        <input type="text" class="form-control" name="name" {{ $errors->has('name') ? 'has-error' : '' }}  value="{{old('name')}}" placeholder="Enter Name">
                                        <span class="help-block"><strong></strong></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile Number </label>
                                        <input type="phone" class="form-control" name="mobile" {{ $errors->has('mobile') ? 'has-error' : '' }}  value="{{old('mobile')}}" placeholder="Enter Patient's Mobile">
                                        <span class="help-block"><strong></strong></span> 
                                        </div>
                                        <div class="form-group">
                                            <label>Age </label>
                                        <input type="text" class="form-control" name="age" {{ $errors->has('age') ? 'has-error' : '' }}  value="{{old('age')}}" placeholder="Enter Patient's age">
                                        <span class="help-block"><strong></strong></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleSelect2">Treatment</label>
                                            <select multiple="" class="form-control" name="treat" id="exampleSelect2">
                                                <option>A</option>
                                                <option>B</option>
                                                <option>C</option>
                                                <option>D</option>
                                            </select>
                                            <span class="help-block"><strong></strong></span>
                                        </div>
                                    </div>
                                    
                                
        
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" id="closepatient" data-dismiss="modal">Close</button>
                                <button type="button" id="patientsubmit" class="btn btn-brand ">Save changes</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                    <!--begin: Datatable --> 
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="tablePatient">
                       
                    </table>
                    <ul id="pagination" class="pagination-sm"></ul>
                   
                    <!--end: Datatable -->
                </div>
            </div>
            
        </div>
        <!-- Edit Item Modal -->
		<div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Patient</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
  
  
                        <form data-toggle="validator" id="editPatient" action="" method="put">
                            <div class="form-group">
                              <label class="control-label" for="name">Name:</label>
                              <input type="text" name="name" class="form-control" data-error="Please enter Name." required />
                              <span class="help-block"><strong></strong></span>
                          </div>
                          <div class="form-group">
                              <label class="control-label" for="mobile">Mobile:</label>
                              <input type="text" name="mobile" class="form-control" data-error="Please enter Mobile." required />
                              <span class="help-block"><strong></strong></span>
                          </div>
                          <div class="form-group">
                            <label class="control-label" for="age">Age:</label>
                            <input type="text" name="age" class="form-control" data-error="Please enter age." required />
                            <span class="help-block"><strong></strong></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="treat">Treatment:</label>
                            <input type="text" name="old" class="form-control" readonly />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label for="exampleSelect2">Treatment</label>
                            <select multiple="" class="form-control" name="treat" id="exampleSelect2">
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                                <option>D</option>
                            </select>
                            <span class="help-block"><strong></strong></span>
                        </div>
                          <div class="form-group">
                              <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                          </div>
                        </form>
  
  
                </div>
              </div>
            </div>
          </div>
        <!-- end:: Content Body -->
@endsection
@section('scripts')
<script >
        
        
        manageData();
        
        
        /* manage data list */
        function manageData() 
        {
            $.ajax({
                dataType: 'json',
                url: '/patients',
                data: {page:page}
            }).done(function(data){
        
                //console.log(data);
                total_page = data.last_page;
                current_page = data.current_page;
                next = data.next_page_url;
                pre= data.pre_page_url;
                last = data.last_page_url;
        
        
                $('#pagination').twbsPagination({
                    totalPages: total_page,
                    visiblePages: current_page,
                    onPageClick: function (event, pageL) {
                        //alert(pageL);
                        page = pageL;
                        if(is_ajax_fire != 0){
                          getPageData();
                        }
                    }
                });
        
        
                manageRow(data.data);
                is_ajax_fire = 1;
            });
        }
        /* Get Page Data*/
        function getPageData() {
            $.ajax({
                dataType: 'json',
                url: '/patients',
                data: {page:page}
            }).done(function(data){
                manageRow(data.data);
            });
        }
        /* Add new Item table row */
        function manageRow(data) {
            //console.log(data);
            var	tabletd = ''; 
            tabletd = '<thead>';
            tabletd  += '<tr>'
            tabletd  +='<th>ID</th>'
            tabletd  += '<th>Name</th>'
            tabletd  += '<th>Mobile</th>'
            tabletd  += '<th>Age</th>'
            tabletd  += '<th>Treatment</th>'
            tabletd  += '<th>Actions</th>'
            tabletd  += '</tr>'
            tabletd  += '</thead>'
            tabletd  +='<tbody >'
            var srno=1;
            $.each( data, function( key, value ) {
               
                tabletd = tabletd + '<tr>';
                tabletd = tabletd + '<td>'+srno+'</td>';
                tabletd = tabletd + '<td>'+value.name+'</td>';
                tabletd = tabletd + '<td>'+value.mobile+'</td>';
                tabletd = tabletd + '<td>'+value.age+'</td>';
                tabletd = tabletd + '<td>'+value.treatment+'</td>';
                tabletd = tabletd + '<td data-id="'+value.id+'">';
                tabletd = tabletd + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
                tabletd = tabletd + '<button class="btn btn-danger remove-item">Delete</button>';
                tabletd = tabletd + '</tr>';
                srno++;
            });
        
            tabletd+='</tbody>';
            $("#tablePatient").html(tabletd);
        }
        //deleting item
        /* Remove Item */
        $("body").on("click",".remove-item",function(){
            var id = $(this).parent("td").data('id');
            var c_obj = $(this).parents("tr");
            $.ajax({
                dataType: 'json',
                type:'delete',
                url: '/patients' + '/' + id,
            }).done(function(data){
                c_obj.remove();
                toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});
                getPageData();
            });
        });
        /* Edit Item */
        $("body").on("click",".edit-item",function(){
            var id = $(this).parent("td").data('id');
            var name = $(this).parent("td").prev("td").prev("td").prev("td").prev("td").text();
            var mobile = $(this).parent("td").prev("td").prev("td").prev("td").text();
            var age = $(this).parent("td").prev("td").prev("td").text();
            var old = $(this).parent("td").prev("td").text();
            var treat = $(this).parent("td").text();
           //alert(treat);
            $("#edit-item").find("input[name='name']").val(name);
            $("#edit-item").find("input[name='mobile']").val(mobile);
            $("#edit-item").find("input[name='age']").val(age);
             $("#edit-item").find("input[name='old']").val(old);
            $("#edit-item").find("input[name='treat']").val(treat);
            $("#edit-item").find("form").attr("action",'/patients' + '/' + id);
        });
        /* Updated new Item */
        $(".crud-submit-edit").click(function(e){
            e.preventDefault();
            var form_action = $("#edit-item").find("form").attr("action");
            var name = $("#edit-item").find("input[name='name']").val();
            var mobile = $("#edit-item").find("input[name='mobile']").val();
            var age = $("#edit-item").find("input[name='age']").val();
            var old = $("#edit-item").find("input[name='old']").val();
            var treat = $("#edit-item").find("select[name='treat']").val();
        alert(old);
        
            $.ajax({
                dataType: 'json',
                type:'PUT',
                url: form_action,
                data:{name:name, mobile:mobile, age:age, old:old,treat:treat}
            }).done(function(data){
               
                if($.isEmptyObject(data.error)){
                                
                    getPageData();
                $(".modal").modal('hide');
                toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
                            }else{
                                $.each(data.error, function (key, value) {
                                    var input = '#editPatient input[name=' + key + ']';
                                    $(input + '+span>strong').text(value);
                                     //$(input).parent().parent().addClass('has-error');
                                });
                            }
            });
        });
            $(document).ready(function() {
                $("#patientsubmit").click(function(e){
                    e.preventDefault();
                    $('input+span>strong').text('');
                    
                    $('input').parent().parent().removeClass('has-error');
                    var _token = $("input[name='_token']").val();
                    var name = $("input[name='name']").val();
                    var mobile = $("input[name='mobile']").val();
                    var age = $("input[name='age']").val();
                    var treat = $("select[name='treat']").val();
                   // alert(treat);
                    var data ={
                        '_token' : _token,
                        'name' : name,
                        'mobile' : mobile,
                        'age' : age,
                        'treat':treat
                    }
                            
                    $.ajax({
                        url: "{{url('/patients')}}",
                        data: data,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(data) {
                            $("#addPatient")[0].reset();
                            // console.log(data);
                            if($.isEmptyObject(data.error)){
                                
                                $("#closepatient").click();
                                manageData();
                                $("#alertpatient").show();
                                $("#patientmsg").val(data.success);
                            }else{
                                $.each(data.error, function (key, value) {
                                    var input = '#addPatient input[name=' + key + ']';
                                    $(input + '+span>strong').text(value);
                                     //$(input).parent().parent().addClass('has-error');
                                });
                            }
                        }
                    });
                }); 
            });
                
            //user    
            $(document).on('click','.open_modal',function(){
                var user_id = $('.open_modal').attr('id');
               //alert(product_id);
                $.get('/users' + '/' + user_id, function (data) {
                    //success data
                    //alert(data.image);
                    $('#user_id').val(data.id);
                    $('#name').val(data.name);
                    $('#email').val(data.email);
                    if(data.image=='NO'){
                        $('#image').attr('src','http://127.0.0.1:8000/assets/media/users/default.jpg');
                    }else{
                        $('#image').attr('src','{{'+data.image+'}}');
                    }
                 
                    $('#role').val(data.role);
                    $('#upuser').val("update");
                    $('#myModal').modal('show');
                }) 
            });
        
            $(document).ready(function () {
           $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
        });
        function checkPasswordMatch() {
            var password = $("#txtNewPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();
        
            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("Passwords do not match!").addClass('text-danger');
            else
                $("#divCheckPasswordMatch").html("Passwords match.");
        }
            </script>
@endsection