@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="k-portlet">
                        <div class="k-portlet__head">
                            <div class="k-portlet__head-label">
                                <h3 class="k-portlet__head-title">Add Patients</h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="k-form" method="POST" action="/patients">
                            @csrf
                            @method('POST')
                            <div class="k-portlet__body">
                                
                                <div class="form-group">
                                    <label>Name</label>
                                <input type="text" class="form-control" name="name" {{ $errors->has('name') ? 'has-error' : '' }}  value="{{old('name')}}" placeholder="Enter Patient's Name">
                                    @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Mobile Number </label>
                                <input type="text" class="form-control" name="mobile" {{ $errors->has('mobile') ? 'has-error' : '' }}  value="{{old('mobile')}}" placeholder="Enter Patient's Mobile">
                                    @if($errors->has('mobile'))
                                    <span class="text-danger">{{ $errors->first('mobile') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Treatments</label>
                                    <div class="k-checkbox-inline">
                                        <label class="k-checkbox">
                                            <input type="checkbox" name="treat[]"  value="ABC"> ABC
                                            <span></span>
                                        </label>
                                        <label class="k-checkbox">
                                            <input type="checkbox" name="treat[]" value="XYZ"> XYZ
                                            <span></span>
                                        </label>
                                        <label class="k-checkbox">
                                            <input type="checkbox" name="treat[]" value="DEC"> DEC
                                            <span></span>
                                        </label>
                                    </div>
                                    <span class="form-text text-muted">Some help text goes here</span>
                                </div>
                                
                            </div>
                            <div class="k-portlet__foot">
                                <div class="k-form__actions">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content Body -->
@endsection