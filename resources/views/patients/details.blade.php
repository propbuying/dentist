@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
           
        <div class="k-portlet k-portlet--mobile">
            <div class="k-portlet__head">
                <div class="k-portlet__head-label">
                    <h3 class="k-portlet__head-title">
                        List Of Patient Details
                    </h3>
                </div>
            </div>
            <!--begin::Form-->
            <form class="k-form" id="pdetailsForm">
                
                <div class="k-portlet__body">
                    <div class="k-repeater">
                        <div data-repeater-list="demo2">
                            
                            <div data-repeater-item class="k-repeater__item">
                                <h3 class="k-heading k-heading--md k-heading--no-top-margin">Treatment Info</h3>
                                <div class="form-group row">
                                   
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <label >Treatment</label>
                                        <select class="form-control k_selectpicker" data-live-search="true"  id="reposelect0"  onchange="myFunction()" >
                                                <option  value="0">Select Treatment</option> 
                                        </select>
                                        
                                    </div>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                            <label >Sub-Treatment</label>
                                            {{-- <select class="form-control k_selectpicker" data-live-search="true">
                                                <option>Hot Dog, Fries and a Soda</option>
                                                <option>Burger, Shake and a Smile</option>
                                                <option>Sugar, Spice and all things nice</option>
                                            </select> --}}
                                            <select class="form-control k_selectpicker" data-live-search="true"  id="reposelectsub0">
                                                    
                                            </select>
                                           
                                    </div>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                            <label >Stage</label>
                                            <select class="form-control k_selectpicker" data-live-search="true" >
                                                <option  value="1">Stage 1</option>
                                                <option  value="2">Stage 2</option>
                                                <option  value="3">Final Stage</option>
                                            </select>
                                           
                                    </div>
                                </div>
                                <div class="form-group row">
                                        <div class="col-lg-4 col-md-9 col-sm-12">
                                            <label>Price:</label>
                                            <input type="text" class="form-control" id="sprice" placeholder="Treatment Price" readonly>
                                        </div>
                                        <div class="col-lg-4 col-md-9 col-sm-12">
                                            <label>Discount:</label>
                                            <select class="form-control k_selectpicker" data-live-search="true" id="subDiscount">
                                               
                                               
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-9 col-sm-12">
                                            <label>Total Price:</label>
                                            <input type="text" class="form-control" name="total" id="total" placeholder="Total Price">
                                            <span id="errmsg" class="help-block"></span>
                                        </div>
                                       
                                </div>
                               
                                <div class="k-separator k-separator--border-dashed"></div>
                                <div class="k-separator k-separator--height-sm"></div>
                            </div>
                        </div>
                        <span id="separate0"></span>
                        <div class="k-repeater__add-data">
                            <span  class="btn btn-success  btn-sm " id="addrep">
                                <i class="la la-plus"></i> Add Another Contact
                            </span>
                        </div>
                    </div>
                </div>
                <div class="k-portlet__foot">
                    <div class="k-form__actions k-form__actions--right">
                        <div class="row">
                            <div class="col k-align-left">
                                <button type="submit" class="btn btn-brand">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
<!--begin::Page Scripts -->
<script src="{{ asset('assets/demo/default/custom/components/forms/layouts/repeater.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
<!--end::Page Scripts -->

<script>
    var page=1;
    function myFunction(){
       // alert(this);
           var dataid= $(this).find(':selected').data('id');
           alert(dataid);
       }
    $(document).ready(function(){   
        var  count=0;
        //repeater with diffrent fields id
        $("#addrep").click(function(){

            count++;
           // alert(count);
            var output = '<div data-repeater-item class="k-repeater__item"><h3 class="k-heading k-heading--md k-heading--no-top-margin">Treatment Info</h3><div class="form-group row"><div class="col-lg-4 col-md-9 col-sm-12"><label >Treatment</label><select class="form-control k_selectpicker treat" data-live-search="true" id="reposelect'+count+'" data-id="'+count+'"  onchange="myFunction()"><option  value="0">Select Treatment</option></select></div><div class="col-lg-4 col-md-9 col-sm-12"><label >Sub-Treatment</label><select class="form-control k_selectpicker" data-live-search="true"  id="reposelectsub'+count+'"></select></div><div class="col-lg-4 col-md-9 col-sm-12"><label >Stage</label><select class="form-control k_selectpicker" data-live-search="true" id="stage'+count+'"><option  value="1">Stage 1</option><option  value="2">Stage 2</option><option  value="3">Final Stage</option></select></div></div><div class="form-group row"><div class="col-lg-4 col-md-9 col-sm-12"><label>Price:</label><input type="text" class="form-control" id="sprice'+count+'" placeholder="Treatment Price" readonly></div><div class="col-lg-4 col-md-9 col-sm-12"><label>Discount:</label><select class="form-control k_selectpicker" data-live-search="true" id="subDiscount'+count+'"></select></div><div class="col-lg-4 col-md-9 col-sm-12"><label>Total Price:</label><input type="text" class="form-control" name="total" id="total'+count+'" placeholder="Total Price"><span id="errmsg" class="help-block"></span></div></div><div class="k-repeater__data form-group"><span  class="btn btn-warning  btn-sm" id="remove'+count+'"><i class="la la-close"></i> Remove</span></div><div class="k-separator k-separator--border-dashed"></div><div class="k-separator k-separator--height-sm"></div></div><span id="separate'+count+'"></span>';
            //var separatorspan '<span id="separate'+count+'"></span>';
            //alert(output);
            var sep = count - 1;
            $('#separate'+sep).append(output);
            //$('#separate'+sep).after(separatorspan);
            getTreatment();


            //    function getrTreatment(){
            //        $.ajax({
            //            dataType: 'json',
            //            url:'/alltreats',
            //            data: {page:page}
            //        }).done(function(data){
            //           // console.log(data.id);
            //          ///$('#reposelect').append( $('<option></option>').val('0').html('select') );
            //            $.each( data, function( key, value ) {
            //                console.log(value.id);
            //               $('#reposelect'+count).append($('<option></option>').val(value.id).html(value.name) );
            //               $('#reposelect'+count).selectpicker('refresh');
            //            });
            //        });
                
            //    }
        });
        
        //collect treatments
        function getTreatment(){
            $.ajax({
                dataType: 'json',
                url:'/alltreats',
                data: {page:page}
            }).done(function(data){
                // console.log(data.id);
                ///$('#reposelect').append( $('<option></option>').val('0').html('select') );
                $.each( data, function( key, value ) {
                    //console.log(value.id);
                    $('#reposelect'+count).append($('<option id='+count+'></option>').val(value.id).html(value.name) );
                    $('#reposelect'+count).selectpicker('refresh');
                });
            });
            
        }
        //CALL TREATMENTS
        getTreatment();
        
        //ONCHAGE TREATMENTS
        $('select').change(function(){
            var dataid= $(this).find(':selected').data('id');
            console.log(dataid);
            var tid = $(this).children("option:selected").val();
            getSTreatment(tid,1);
           
        });
        
        //ON CHANGE SUB-TREATMENTS
        $("#reposelectsub0").change(function(){
           //$('#subDiscount').append($('<option></option>').val('0').html('Select') );
            var sid = $(this).children("option:selected").val();
            getsprice(sid);
            getDiscount();
        });   
        //COLLECT SUB-TREATMENTS
        function getSTreatment(id,subid){
            //alert(subid);
            $.ajax({
                dataType: 'json',
                url:'/allsubtreat',
                data: {id:id}
            }).done(function(data){
                $('#reposelectsub'+subid).empty()
                $('#reposelectsub'+subid).append( $('<option></option>').val('0').html('select') );
                $.each( data, function( key, value ) {
                    $('#reposelectsub'+subid).append($('<option></option>').val(value.id).html(value.name) );
                    $('#reposelectsub'+subid).selectpicker('refresh');
                });
            });
        }
        //COLLECT PRICE
        function getsprice(id){
            $.ajax({
                dataType: 'json',
                url:'/subprice',
                data: {id:id}
            }).done(function(data){
           // $('#reposelectsub').empty()
                $.each( data, function( key, value ) {
                $('#sprice').val(data.price);
                });
            });
        }
        //COLLECT Discount
        function getDiscount(){
                $('#subDiscount').empty().append($('<option></option>').val(0).html('Select') );
                $('#subDiscount').append($('<option></option>').val(10).html('10%') );
                $('#subDiscount').selectpicker('refresh');
            }
        //Discount on change
       $("#subDiscount").change(function(){
            var dval = $(this).children("option:selected").val();
            var price = $("#sprice").val();
            var total = price - (price * (dval/100));
            $("#total").val(total);
            //alert(total);
            //getsprice(sid);
        });

        //CACULATE TOTAL PRICE
        $("#total").keyup(function(){
            //alert(e);
            var final = $("#total").val();
            // if (final.which != 8 && final.which != 0 && (final.which < 48 || final.which > 57)) {
            //     //display error message
            //   //  $("#errmsg").html("Digits Only").show().delay(2000).fadeOut("slow");
            //    return false;
            // }
            var discount = $("#subDiscount").children("option:selected").val();
            //console.log(dval);
            var original = Math.round(final/(1-(discount/100)));
            $("#sprice").val(original);
           //console.log(original);
        });
    });
    
</script>

@endsection