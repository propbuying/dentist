@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            @if(session('msg'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('msg')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
            <div class="k-portlet k-portlet--mobile">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title">
                            List Of Roles
                        </h3>
                    </div>
                </div>
                <div class="k-portlet__body">
                <div class="col-md-4">
                    <a href="/roles/create" class="btn btn-info">Add New</a>
                </div>
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="k_table_1">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                                @if(count($roles)>0)
                                @foreach($roles as $role)
                                <tr>
                                        <td>{{$role->id}}</td>
                                        <td>{{$role->name}}</td>
                                        <td >
                                            <a href="/roles/{{$role->id}}/permission" title="Click to give permissions"><i class="flaticon-interface-4"></i></a> |  <a href="/roles/{{$role->id}}/edit"><i class="flaticon2-edit-interface-symbol-of-pencil-tool"></i></a> | <a href="javascript:void(0)" data-id="{{$role->id}}" class="k_sweetalert_demo_8"><i class="flaticon-delete"></i></a>
                                            <form method="post" action="/rolesdestroy/{{$role->id}}" class="role_{{$role->id}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif        
                        </tbody>
                    </table>

                    <!--end: Datatable -->
                </div>
            </div>
            
        </div>

        <!-- end:: Content Body -->
@endsection