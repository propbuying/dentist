@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="k-portlet">
                        <div class="k-portlet__head">
                            <div class="k-portlet__head-label">
                                <h3 class="k-portlet__head-title">Edit Role</h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        
                        <form class="k-form" method="POST" action="{{url('/roles/'.$roles->id)}}">
                                @csrf
                                @method('PATCH')
                                <div class="k-portlet__body">
                                    <div class="form-group">
                                        <label>Role</label>
                                    <input type="text" class="form-control {{ $errors->has('name') ? 'has-error' : '' }} " name="name"  value="{{$roles->name}}" placeholder="Enter role">
                                        @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="k-portlet__foot">
                                    <div class="k-form__actions">
                                        <button type="submit" class="btn btn-primary">Edit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </form>  
                      
                       

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content Body -->
@endsection