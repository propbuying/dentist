@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="k-portlet">
                        <div class="k-portlet__head">
                            <div class="k-portlet__head-label">
                                <h3 class="k-portlet__head-title">Add Role</h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="k-form" method="POST" action="/roles">
                            @csrf
                            @method('POST')
                            <div class="k-portlet__body">
                                <div class="form-group form-group-last">
                                        @if(session('msg'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            {{session('msg')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          @endif
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                <input type="text" class="form-control" name="name" {{ $errors->has('name') ? 'has-error' : '' }}  value="{{old('name')}}" placeholder="Enter role">
                                    @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="k-portlet__foot">
                                <div class="k-form__actions">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content Body -->
@endsection