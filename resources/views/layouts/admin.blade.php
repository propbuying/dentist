<!DOCTYPE html>
<html lang="en">
    <head>
        @include('partials.head')
    </head>
    <body class="k-header--fixed k-header-mobile--fixed k-aside--enabled k-aside--fixed">
        <!-- begin:: Header Mobile -->
		<div id="k_header_mobile" class="k-header-mobile  k-header-mobile--fixed ">
            <div class="k-header-mobile__logo">
                <a href="index.html">
                    <img alt="Logo" src="{{ asset('assets/media/logos/logo-6.png') }}" />
                </a>
            </div>
            <div class="k-header-mobile__toolbar">
                <button class="k-header-mobile__toolbar-toggler k-header-mobile__toolbar-toggler--left" id="k_aside_mobile_toggler"><span></span></button>
                <button class="k-header-mobile__toolbar-toggler" id="k_header_mobile_toggler"><span></span></button>
                <button class="k-header-mobile__toolbar-topbar-toggler" id="k_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>
        <div class="k-grid k-grid--hor k-grid--root">
            <div class="k-grid__item k-grid__item--fluid k-grid k-grid--ver k-page">
                @include('partials.sidebar')
                <div class="k-grid__item k-grid__item--fluid k-grid k-grid--hor k-wrapper" id="k_wrapper">
                    @include('partials.header')
                     <!-- begin:: Content -->
                    <div class="k-content	k-grid__item k-grid__item--fluid k-grid k-grid--hor" id="k_content">
                        @yield('content')
                    </div>
                    <!-- end:: Content -->
                    <!-- begin:: Footer -->
                    <div class="k-footer	k-grid__item k-grid k-grid--desktop k-grid--ver-desktop">
                            <div class="k-footer__copyright">
                                2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/keen" target="_blank" class="k-link">TaskPanel</a>
                            </div>
                            <div class="k-footer__menu">
                                <a href="http://keenthemes.com/keen" target="_blank" class="k-footer__menu-link k-link">About</a>
                                <a href="http://keenthemes.com/keen" target="_blank" class="k-footer__menu-link k-link">Team</a>
                                <a href="http://keenthemes.com/keen" target="_blank" class="k-footer__menu-link k-link">Contact</a>
                            </div>
                    </div>
                    <!-- end:: Footer -->
                </div>
            </div>
        </div>
        @include('partials.javascripts')
        
    </body>

</html>