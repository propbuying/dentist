@extends('layouts.admin')
@section('content')
<style>
	#tpage{
  display: inline-block;
  margin-bottom: 1.75em;
}
#tpage li{
  display: inline-block;
  margin:10px;
  border:1px solid gray;
  padding:5px;
}
</style>
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
           
            <div class="k-portlet k-portlet--mobile">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title">
                            List Of Treatments
                        </h3>
                    </div>
                </div>
                <div class="k-portlet__body">
                        <div class='alert alert-success fade show' role='alert' style="display:none;">
                                <div class='alert-icon'><i class='fa fa-check'></i></div>
                                <div class='alert-text' id="msgtreat"></div>
                                <div class='alert-close'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                        <span aria-hidden='true'><i class='la la-close'></i></span>
                                    </button>
                                </div>
                            </div>
              
               <div class="col-md-4">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#treatmodal" class="btn btn-info">Add Treatment</a>
                </div>
                <!-- Modal -->
               <div class="modal fade" id="treatmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Treatemnt</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="addtreat" class="k-form" >
                                    @csrf
                                    
                                    <div class="k-portlet__body">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Enter Name">
                                            <span class="help-block"><strong></strong></span>
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" id="closetreat" data-dismiss="modal">Close</button>
                                <button type="button" id="treatsubmit" class="btn btn-brand ">Save changes</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                    <!--begin: Datatable --> 
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="tableTreats">
                       
                    </table>
                    <ul id="tpage" class="pagination-sm"></ul>
                   
                    <!--end: Datatable -->
                </div>
            </div>
            
        </div>
        <!-- Edit Item Modal -->
		<div class="modal fade" id="edittreat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Patient</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
  
  
                        <form data-toggle="validator" id="edittreat" action="" method="put">
                            <div class="form-group">
                                <label class="control-label" for="name">Name:</label>
                                <input type="text" name="name" class="form-control" data-error="Please enter treatment name." required />
                                <span class="help-block"><strong></strong></span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success crud-treat">Submit</button>
                            </div>
                        </form>
  
  
                </div>
              </div>
            </div>
          </div>
        <!-- end:: Content Body -->
@endsection

@section('scripts')
<script>
    var url = '/treatments';
    managetreat();
    /* manage data list */
    function managetreat() 
    {
        $.ajax({
            dataType: 'json',
            url:url,
            data: {page:page}
        }).done(function(data){

            //console.log(data);
            total_page = data.last_page;
            current_page = data.current_page;
            next = data.next_page_url;
            pre= data.pre_page_url;
            last = data.last_page_url;


            $('#tpage').twbsPagination({
                totalPages: total_page,
                visiblePages: current_page,
                onPageClick: function (event, pageL) {
                    //alert(pageL);
                    page = pageL;
                    if(is_ajax_fire != 0){
                    getPageData();
                    }
                }
            });


            manageRow(data.data);
            is_ajax_fire = 1;
        });
    }

    /* Get Page Data*/
    function getPageData() {
            $.ajax({
                dataType: 'json',
                url: url,
                data: {page:page}
            }).done(function(data){
                manageRow(data.data);
            });
        }

         /* Add new Item table row */
         function manageRow(data) {
            //console.log(data);
            var	tabletd = ''; 
            tabletd = '<thead>';
            tabletd  += '<tr>'
            tabletd  +='<th>ID</th>'
            tabletd  += '<th>Name</th>'
            tabletd  += '<th>Actions</th>'
            tabletd  += '</tr>'
            tabletd  += '</thead>'
            tabletd  +='<tbody >'
            var srno=1;
            $.each( data, function( key, value ) {
               
                tabletd = tabletd + '<tr>';
                tabletd = tabletd + '<td>'+srno+'</td>';
                tabletd = tabletd + '<td>'+value.name+'</td>';
                tabletd = tabletd + '<td data-id="'+value.id+'">';
                tabletd = tabletd + '<button data-toggle="modal" data-target="#edittreat" class="btn btn-primary edittreat">Edit</button> ';
                tabletd = tabletd + '<button class="btn btn-danger removetreat">Delete</button>';
                tabletd = tabletd + '</tr>';
                srno++;
            });
        
            tabletd+='</tbody>';
            $("#tableTreats").html(tabletd);
        }
        //deleting item
        /* Remove Item */
        $("body").on("click",".removetreat",function(){
            var id = $(this).parent("td").data('id');
            var c_obj = $(this).parents("tr");
            $.ajax({
                dataType: 'json',
                type:'delete',
                url:url + '/' + id,
            }).done(function(data){
                c_obj.remove();
                toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});
                getPageData();
            });
        });
        /* Edit Item */
        $("body").on("click",".edittreat",function(){
            var id = $(this).parent("td").data('id');
            var name = $(this).parent("td").prev("td").text();
            alert(name);
           $("#edittreat").find("input[name='name']").val(name);
           $("#edittreat").find("form").attr("action",url + '/' + id);
        });
        /* Updated new Item */
        $(".crud-treat").click(function(e){
            e.preventDefault();
            var form_action = $("#edittreat").find("form").attr("action");
            var name = $("#edittreat").find("input[name='name']").val();
        
            $.ajax({
                dataType: 'json',
                type:'PUT',
                url: form_action,
                data:{name:name}
            }).done(function(data){
               
                if($.isEmptyObject(data.error)){
                                
                    getPageData();
                $(".modal").modal('hide');
                toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
                            }else{
                                $.each(data.error, function (key, value) {
                                    var input = '#edittreat input[name=' + key + ']';
                                    $(input + '+span>strong').text(value);
                                     //$(input).parent().parent().addClass('has-error');
                                });
                            }
            });
        });
            $(document).ready(function() {
                $("#treatsubmit").click(function(e){
                    e.preventDefault();
                    $('input+span>strong').text('');
                    
                    $('input').parent().parent().removeClass('has-error');
                    var _token = $("input[name='_token']").val();
                    var name = $("input[name='name']").val();
                   // alert(treat);
                    var data ={
                        '_token' : _token,
                        'name' : name,
                    }
                            
                    $.ajax({
                        url: url,
                        data: data,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(data) {
                            $("#addtreat")[0].reset();
                            // console.log(data);
                            if($.isEmptyObject(data.error)){
                                
                                $("#closetreat").click();
                                managetreat();
                               //$("#alertpatient").show();
                                $("#msgtreat").val(data.success);
                            }else{
                                $.each(data.error, function (key, value) {
                                    var input = '#addtreat input[name=' + key + ']';
                                    $(input + '+span>strong').text(value);
                                     //$(input).parent().parent().addClass('has-error');
                                });
                            }
                        }
                    });
                }); 
            });
</script>
@endsection
