@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            @if(session('msg'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('msg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="k-portlet k-portlet--mobile">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title">
                            List Of Users
                        </h3>
                    </div>
                </div>
                <div class="k-portlet__body">
                <div class="col-md-4">
                    <a href="/users/create" class="btn btn-info">Add New</a>
                </div>
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="k_table_1">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Image</th>
                                <th>Provider</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($users)>0)
                                @foreach($users as $user)
                                <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>@if ($user->image=='NO')
                                                <img src="{{ asset('assets/media/users/default.jpg')}}" alt="User Profile Pic" width="100px"/>
                                        @else
                                            <img src="{{$user->image}}" alt="profile" width="100px">
                                        @endif</td>
                                        <td>{{$user->provider}}</td>
                                        <td>
                                            @empty($user->role)
                                                <a href="/users/{{$user->id}}/role" title="Click to assign role">
                                                        <i class="fa fa-arrow-circle-down"></i>
                                                </a>    
                                            @endempty
                                            @isset($user->role)
                                                <a href="/users/{{$user->id}}/role" title="Click to update role">
                                                    {{$user->role}} <i class="flaticon2-edit-interface-symbol-of-pencil-tool"></i>
                                                </a> 
                                        @endisset
                                        </td>
                                        <td >/users/{{$user->id}}/edit
                                                
                                                <a href="javascript:void(0)" id="{{$user->id}}" class="open_modal"><i class="flaticon2-edit-interface-symbol-of-pencil-tool"></i></a> | <a href="javascript:void(0)" data-id="{{$user->id}}" class="k_sweetalert_demo_8"><i class="flaticon-delete"></i></a>
                                            <form method="post" action="/users/{{$user->id}}" class="user_{{$user->id}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif        
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                 <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Product</h4>
                </div>
                <div class="modal-body">
                <form id="frmProducts" name="frmProducts" enctype="multipart/form-data" class="form-horizontal" novalidate="">
                    <div class="form-group error">
                     <label for="inputName" class="col-sm-3 control-label">Name</label>
                       <div class="col-sm-9">
                        <input type="text" class="form-control has-error" id="name" name="name" placeholder="User Name" value="">
                       </div>
                       </div>
                     <div class="form-group">
                     <label for="inputDetail" class="col-sm-3 control-label">Details</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="email" name="email" placeholder="details" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDetail" class="col-sm-3 control-label">Image</label>
                           <div class="col-sm-9">
                          
                           <img id="image" src="">
                           </div>
                       </div>
                       <div class="form-group">
                        
                           <div class="col-sm-9">
                                <input type="file" class="form-control" id="newimg" name="newimg"  >Upload New Image
                           </div>
                       </div>
                       <div class="form-group">
                            <label for="inputDetail" class="col-sm-3 control-label">Change Password</label>
                               <div class="col-sm-9">
                              
                                    <input type="password" class="form-control" id="txtNewPassword" name="password"  >
                               </div>
                        </div>
                        <div class="form-group">
                                <label for="inputDetail" class="col-sm-3 control-label">Confirmed Password</label>
                                   <div class="col-sm-9">
                                  
                                        <input type="password" class="form-control" id="txtConfirmPassword" name="password" onChange="checkPasswordMatch();" >
                                   </div>
                                   <div class="registrationFormAlert" id="divCheckPasswordMatch" calss="">
                            </div>
                            
                </form>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="upuser" value="add">Save changes</button>
                <input type="hidden" id="user_id" name="user_id" value="0">
                </div>
            </div>
          </div>
      </div>
    <!-- end:: Content Body -->
@endsection