@extends('layouts.admin')
@section('content')
    <!-- begin:: Content Body -->
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="k-portlet">
                        <div class="k-portlet__head">
                            <div class="k-portlet__head-label">
                                <h3 class="k-portlet__head-title">Assign Role</h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="k-form" method="POST" action="/assignrole/{{$userdetails['uid']}}">
                            @csrf
                            @method('POST')
                            <div class="k-portlet__body">
                                <div class="form-group">
                                   
                                    <div class="k-radio-inline">
                                            @foreach ($roles as $role)
                                        <label class="k-radio">
                                        <input type="radio" name="role" value="{{$role->name}}" @if (($role->name)==($userdetails['urole']))
                                            checked 
                                            @endif > {{$role->name}}
                                            <span></span>
                                        </label>
                                    
                                        @endforeach
                                        <input type="hidden" name="prerole" value="{{$userdetails['urole']}}">
                                    </div>
                                    <span class="form-text text-muted">Please Select Role for user</span>
                                </div>
                            </div>
                            <div class="k-portlet__foot">
                                <div class="k-form__actions">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content Body -->
@endsection