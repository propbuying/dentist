<?php

namespace App\Http\Controllers;

use App\Subtreat;
use App\Treatment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubtreatController extends Controller
{
    public function managesubtreats(){
        return view('subtreats.index');
   }
   public function allsubtreat(Request $request){

    //dd($request->id);
    $subtreat = Subtreat::where('treatment_id',$request->id)->get();
    return response()->json($subtreat);  
   }
   public function subprice(Request $request){
    $subtreat = Subtreat::where('id',$request->id)->first();
    return response()->json($subtreat);  
   }
   public function index(){
        $treat = Subtreat::latest()->paginate(5);
      /*  foreach($treat as $treat){
            $treat->treatment_id= Subtreat::find($treat->id)->treatment->name;
        }*/
       // dd($treat);
        return response()->json($treat); 
    }
    public function store(Request $request)
    {
    
        $validator = Validator::make($request->all(), [
         'name' => 'required|min:3',
         'price' => 'required'
        ]);
        if ($validator->passes()) {
            if($request->treatment_id==0){
               $treatid = Treatment::where('name','Not Assigned')->first();
               //dd($treatid->id);
               $request->treatment_id = $treatid->id;
            }
            Subtreat::create([
                'name'=> $request->name,
                'price'=> $request->price,
                'treatment_id' => $request->treatment_id ,
            ]);
            return response()->json(['success'=>'Added new records.']);
        }
    return response()->json(['error'=>$validator->errors()->getMessages()]);
    }
    public function update(Request $request, $id)
    {
       // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3', 
            'price' => 'required',  
        ]);
        if ($validator->passes()) {
            if($request->treatment_id==0){
                $edit = Subtreat::find($id)->update([
                    'name' => $request->name,
                    'price'=> $request->price,
                    ]);
            }else{
                $edit = Subtreat::find($id)->update($request->all());
            }

          
            
        return response()->json($edit);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    
    }
    public function destroy($id){
        //print_r($id);
        Subtreat::find($id)->delete();
        return response()->json(['done']);
    }
}
