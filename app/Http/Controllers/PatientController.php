<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PatientController extends Controller
{
    public function manageAllPatients()
    {
        return view('patients.index');
    }

    public function index(){
        $patient = Patient::latest()->paginate(5);
        //print_r($patient->treatment);
       // $patient->treatment=unserialize($patient->treatment);
        return response()->json($patient);
   }
   public function create(){
    
    return view('patients.create');
}
public function store(Request $request){
    
       //dd($request->treat);
       $validator = Validator::make($request->all(), [
        'name' => 'required|min:3',
        'mobile' => 'required|unique:patients|digits:10',
        'age' => 'required|numeric',
        'treat' => 'required',
    ]);
   

    if ($validator->passes()) {

        Patient::create([
            'name'=> $request->name,
            'mobile' => $request->mobile,
            'age' => $request->age,
            'treatment'=> implode(',',$request->treat),
        ]);
        return response()->json(['success'=>'Added new records.']);
    }


    return response()->json(['error'=>$validator->errors()->getMessages()]);
      
     
    
     

   }
   public function update(Request $request, $id)
   {
      if($request->treat){
        $request['treatment']=implode(',',$request->treat);
      }
      else{
          $request['treatment']=$request->old;
      }
     // print_r($request->treat);
    $validator = Validator::make($request->all(), [
        'name' => 'required|min:3',
        'mobile' => 'required|digits:10',
        'age' => 'required|numeric',
        
    ]);
    if ($validator->passes()) {

        $edit = Patient::find($id)->update($request->all());
       return response()->json($edit);
    }
    return response()->json(['error'=>$validator->errors()->getMessages()]);
   
   }
   public function destroy($id){
       //print_r($id);
    Patient::find($id)->delete();
    return response()->json(['done']);
   }
}
