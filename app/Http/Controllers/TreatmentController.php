<?php

namespace App\Http\Controllers;

use App\Treatment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TreatmentController extends Controller
{
   public function manageAllTreatments(){
        return view('treatments.index');
   }
   public function alltreats(){
    $all = Treatment::where('name','!=','Not Assigned')->get();
    
    return response()->json($all); 
   }
   public function index(){
        $treat = Treatment::latest()->paginate(5);
    
        return response()->json($treat); 
    }
    public function store(Request $request)
    {
    
        $validator = Validator::make($request->all(), [
         'name' => 'required|min:3',
        ]);
        if ($validator->passes()) {
            Treatment::create([
                'name'=> $request->name,
            
            ]);
            return response()->json(['success'=>'Added new records.']);
        }
    return response()->json(['error'=>$validator->errors()->getMessages()]);
    }
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',  
        ]);
        if ($validator->passes()) {

            $edit = Treatment::find($id)->update($request->all());
        return response()->json($edit);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    
    }
    public function destroy($id){
        //print_r($id);
        Treatment::find($id)->delete();
        return response()->json(['done']);
    }
}
