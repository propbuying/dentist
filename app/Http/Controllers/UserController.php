<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('name','!=','Admin')->get();
        foreach($users as $user)
        {
            $roles = $user->getRoleNames();
            if(empty($roles))
            {
                $user['role'] ='NO';
            }else
            {
                foreach($roles as $role)
                {
                    $user['role'] = $role;
                }
            }
            
        }
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
    return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Roles 
    public function allrole(){
        $roles = Role::all();
        return view('roles.index',compact('roles'));
    }
    public function createrole()
    {
        return view('roles.create');
    }
    public function rolestore(Request $request){
        $validator = $request->validate([
            'name' => 'required|unique:roles',
        ]);
        Role::create(['name' => $request->name]);
        return redirect('/roles')->with('msg','Added Successfully');

    } 
    public function roleedit($id){
       
       $roles= Role::find($id);
        return view('roles.edit',compact('roles'));

    }
    public function updaterole(Request $request, $id){
        //dd($id);
        $validator = $request->validate([
            'name' => 'required',
        ]);
       Role::where('id',$id)->update(['name'=>$request->name]);
       return redirect('/roles')->with('msg','Updated Successfully');

    }
    public function rdestroy(Request $request, $id)
    {
        //dd($id);
        Role::find($request->id)->delete();
        return redirect('/roles')->with('msg','User deleted successfully');
    }
    //Assign Permission to users
    public function rolepermission($id){
        $permissions= permission::all();
        $role=role::findById($id);
       $roleid['rid'] = $id;
        return view('roles.permissions',compact('permissions','roleid','role'));
    }
    public function assignpermission(Request $request,$id){
      $role=Role::find($id);
      $role->syncPermissions($request->name);
      
    }
    //assigning role to user
    public function urole($id){
        $roles = Role::all();
        $userdetails['uid']=$id;
        $user = User::find($id);
        $ur = $user->getRoleNames();
        //dd($ur);
            if($ur->isEmpty())
            {
                $userdetails['urole'] ='NO';
            }else
            {
                foreach($ur as $role)
                {
                    $userdetails['urole'] = $role;
                }
            }
           //dd($userdetails['urole']);
            return view('users.role',compact('roles','userdetails'));
    }
    public function assignrole(Request $request,$id){
        // dd($request->role);
        $validate = $request->validate([
             'role'=> 'required'
         ]);
         $user = User::find($id);
         if($request->prerole != 'NO')
         {
             $user->syncRoles($request->role);
         }else{
             $user->assignRole($request->role);
         }
         
         return redirect('users')->with('msg','Role has been assign successfully to '.$user->name);
 
     }
}
