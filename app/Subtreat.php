<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtreat extends Model
{
    public $fillable = ['treatment_id','name','price'];
    
    public function treatment(){
        return $this->belongsTo('App\Treatment');
    }
}
