<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    public $fillable = ['name'];

    public function subtreat(){
        return $this->hasMany('App\Subtreat');
    }
}
