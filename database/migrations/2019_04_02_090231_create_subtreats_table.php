<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtreatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtreats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('treatment_id')->unsigned()->index();
            $table->foreign('treatment_id')->references('id')->on('treatments')->onDelete('cascade');
            $table->text('name');
            $table->integer('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtreats');
    }
}
