<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//roles
Route::get('/roles','UserController@allrole');
Route::get('/roles/create','UserController@createrole');
Route::post('/roles','UserController@rolestore');
Route::get('/roles/{id}/edit','UserController@roleedit');
Route::patch('/roles/{id}','UserController@updaterole');
Route::delete('/rolesdestroy/{id}','UserController@rdestroy');

//Assign Role
Route::get('/users/{id}/role','UserController@urole');
Route::post('/assignrole/{id}','UserController@assignrole');

//modules
Route::resource('/modules','ModuleController');

//users
Route::resource('/users','UserController');



//patients
Route::get('/allpatients','PatientController@manageAllPatients');
Route::resource('/patients','PatientController');
//Patients details
Route::group(['prefix'=>'patients'],function(){
    Route::apiResource('/{patient}/details','PatientDetailController');
});


//treatments
Route::get('/alltreats','TreatmentController@alltreats');
Route::get('/managetreatments','TreatmentController@manageAllTreatments');
Route::resource('/treatments','TreatmentController');
//Sub-treatments
Route::get('/managesubtreats','SubtreatController@managesubtreats');
Route::resource('/subtreatments','SubtreatController');
Route::get('/allsubtreat','SubtreatController@allsubtreat');
Route::get('/subprice','SubtreatController@subprice');